import unittest
from bfs_grid.dungeon_solve import read_data, solve


class TestSolveDungeons(unittest.TestCase):

    def test_dungeon0(self):
        start_char = "S"
        end_char = "E"
        blocked_char = "#"
        dungeon_file = "bfs_grid\dungeon0.txt"

        data = read_data(dungeon_file)
        steps = solve(data, start_char, end_char, blocked_char)
        self.assertEqual(steps, 9)

    def test_dungeon1(self):
        start_char = "S"
        end_char = "E"
        blocked_char = "#"
        dungeon_file = "bfs_grid\dungeon1.txt"

        data = read_data(dungeon_file)
        steps = solve(data, start_char, end_char, blocked_char)
        self.assertEqual(steps, 13)


if __name__ == "__main__":
    unittest.main()