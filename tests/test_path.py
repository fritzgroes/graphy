import unittest
from graphy.utils import map_to_node_ids
from graphy.path import bellman_ford_shortest, eulerian_path, single_source_longest_path, single_source_shortest_path, \
    dijkstra_distances, dijkstra_shortest, shortest_route_bf, shortest_route, out_degrees, in_degrees, \
    calc_degree_diffs, is_eulerian_circuit, is_eulerian_path

labels = [chr(i) for i in range(ord("A"), ord("M") + 1)]
label_to_int_dict = {label: labels.index(label) for label in labels}

def get_dag1():
    dag_with_labels = [
        [("B", 3), ("C", 6)], 
        [("C", 4), ("D", 4), ("E", 11)], 
        [("D", 8), ("G", 11)], 
        [("E", -4), ("F", 5), ("G", 2)], 
        [("H", 9)],
        [("H", 1)],
        [("H", 2)],
        []
    ]
    return map_to_node_ids(lambda id: label_to_int_dict[id], dag_with_labels)

def get_dag2():
    # switched position of A and D
    dag_with_labels = [
        [("E", -4), ("F", 5), ("G", 2)], 
        [("C", 4), ("A", 4), ("E", 11)], 
        [("A", 8), ("G", 11)], 
        [("B", 3), ("C", 6)], 
        [("H", 9)],
        [("H", 1)],
        [("H", 2)],
        []
    ]
    return map_to_node_ids(lambda id: label_to_int_dict[id], dag_with_labels)


class TestShortestPath(unittest.TestCase):

    graph1 = [[(1, 4), (2, 1)], [(3, 1)], [(1, 2), (3, 5)], [(4, 3)], []]

    def test_shortest_path(self):
        shortest_path = single_source_shortest_path(get_dag1())
        self.assertListEqual(shortest_path, [0, 3, 6, 7, 3, 12, 9, 11])

    def test_shortest_path2(self):
        shortest_path = single_source_shortest_path(get_dag2())
        self.assertListEqual(shortest_path, [7, 3, 6, 0, 3, 12, 9, 11])

    def test_dijkstra_distances(self):
        distances, previous = dijkstra_distances(self.graph1, 0)
        self.assertListEqual(distances, [0, 3, 1, 4, 7])
        self.assertListEqual(previous, [None, 2, 0, 1, 3])

    def test_dijkstra_distances_early(self):
        distances, previous = dijkstra_distances(self.graph1, 0, end=1)
        self.assertListEqual(distances, [0, 3, 1, 4, float("inf")])
        self.assertListEqual(previous, [None, 2, 0, 1, None])

    def test_dijkstra_distances_dense(self):
        graph = [[(1, 5), (2, 1)], [(2, 2), (3, 3), (4, 20)], [(1, 3),(4, 12)], [(2, 3), (4, 2), (5, 6)], [(5, 1)], []]
        distances, previous = dijkstra_distances(graph, 0)
        self.assertListEqual(distances, [0, 4, 1, 7, 9, 10])
        self.assertListEqual(previous, [None, 2, 0, 1, 3, 4])

    def test_dijkstra_shortest1(self):
        path = dijkstra_shortest(self.graph1, 0, 4)
        self.assertListEqual(path, [0, 2, 1, 3, 4])

    def test_dijkstra_shortest2(self):
        path = dijkstra_shortest(self.graph1, 0, 1)
        self.assertListEqual(path, [0, 2, 1])
    
    def test_bellman_ford_shortest(self):
        graph3 = [[(1, 5)], [(2, 20), (5, 30), (6, 60)], [(3, 10), (4, 75)], [(2, -15)], [(9, 100)], 
            [(4, 25), (6, 5), (8, 50)], [(7, -50)], [(8, -10)], [], []]
        distances = bellman_ford_shortest(graph3, 0)
        self.assertListEqual(distances, 
            [0, 5, float("-inf"), float("-inf"), float("-inf"), 35, 40, -10, -20, float("-inf")])
        distances = bellman_ford_shortest(graph3, 0, distances=distances)
        self.assertListEqual(distances, 
            [0, 5, float("-inf"), float("-inf"), float("-inf"), 35, 40, -10, -20, float("-inf")])


class TestLongestPath(unittest.TestCase):

    def test_longest_path(self):
        longest_path = single_source_longest_path(get_dag1())
        self.assertListEqual(longest_path, [0, 3, 7, 15, 14, 20, 18, 23])

    def test_longest_path2(self):
        longest_path = single_source_longest_path(get_dag2())
        self.assertListEqual(longest_path, [15, 3, 7, 0, 14, 20, 18, 23])


class TestTravelingSalesman(unittest.TestCase):

    adj_mat = [[0, 4, 1, 9], [3, 0, 6, 11], [4, 1, 0, 2], [6, 5, -4, 0]]

    def test_shortest_route_bf(self):
        cost, path = shortest_route_bf(self.adj_mat, 0)
        self.assertAlmostEqual(cost, 9, 5)
        self.assertListEqual(path, [0, 3, 2, 1, 0])

    def test_shortest_route(self):
        cost, path = shortest_route(self.adj_mat, 0)
        self.assertAlmostEqual(cost, 9, 5)
        self.assertListEqual(path, [0, 3, 2, 1, 0])


class TestEulerianPath(unittest.TestCase):

    directed_graph1 = [[], [2, 3], [2, 4, 4], [1, 2, 5], [3, 6], [6], [3]]

    def test_out_degrees(self):
        self.assertListEqual(list(out_degrees(self.directed_graph1)), [0, 2, 3, 3, 2, 1, 1])
        
    def test_in_degrees(self):
        self.assertListEqual(list(in_degrees(self.directed_graph1)), [0, 1, 3, 3, 2, 1, 2])
        
    def test_degree_diffs(self):
        self.assertListEqual(list(calc_degree_diffs(self.directed_graph1)), [0, 1, 0, 0, 0, 0, -1])
        
    def test_is_eulerian_circuit(self):
        self.assertFalse(is_eulerian_circuit(self.directed_graph1))
        
    def test_is_eulerian_path(self):
        self.assertTrue(is_eulerian_path(self.directed_graph1))

    def test_eulerian_path(self):
        self.assertListEqual(list(eulerian_path(self.directed_graph1)), [1, 3, 5, 6, 3, 2, 4, 3, 1, 2, 2, 4, 6])


if __name__ == "__main__":
    unittest.main()