from typing import Any, Tuple
import unittest
from graphy.datastructures import IndexedPriorityQueue, PriorityQueue, SparseTableAssociative, SparseTableOverlapFriendly, UnionFind


class TestUnionFind(unittest.TestCase):

    def test_union_find(self):
        uf = UnionFind(7)
        self.assertFalse(uf.connected(0, 1))
        uf.unify(0, 1)
        self.assertTrue(uf.connected(0, 1))
        self.assertFalse(uf.connected(0, 2))
        uf.unify(0, 2)
        self.assertTrue(uf.connected(0, 2))
        self.assertTrue(uf.connected(1, 2))

        self.assertFalse(uf.connected(3, 4))
        uf.unify(3, 4)
        self.assertTrue(uf.connected(3, 4))
        self.assertFalse(uf.connected(0, 3))

        self.assertFalse(uf.connected(5, 6))
        uf.unify(5, 6)
        self.assertTrue(uf.connected(5, 6))
        self.assertFalse(uf.connected(0, 5))
        self.assertFalse(uf.connected(3, 5))

        uf.unify(0, 3)
        self.assertTrue(uf.connected(0, 3))
        self.assertTrue(uf.connected(0, 4))
        self.assertTrue(uf.connected(1, 3))
        self.assertTrue(uf.connected(1, 4))
        self.assertTrue(uf.connected(2, 3))
        self.assertTrue(uf.connected(2, 4))

        uf.unify(4, 5)
        self.assertTrue(uf.connected(0, 3))
        self.assertTrue(uf.connected(0, 4))
        self.assertTrue(uf.connected(1, 3))
        self.assertTrue(uf.connected(1, 4))
        self.assertTrue(uf.connected(2, 3))
        self.assertTrue(uf.connected(2, 4))
        self.assertTrue(uf.connected(3, 5))
        self.assertTrue(uf.connected(3, 6))
        self.assertTrue(uf.connected(4, 5))
        self.assertTrue(uf.connected(4, 6))
        self.assertTrue(uf.connected(0, 5))
        self.assertTrue(uf.connected(0, 6))
        self.assertTrue(uf.connected(1, 5))
        self.assertTrue(uf.connected(1, 6))
        self.assertTrue(uf.connected(2, 5))
        self.assertTrue(uf.connected(2, 6))

    def test_union_find2(self):
        num_vertices = 5
        uf = UnionFind(num_vertices)
        uf.unify(0, 1)
        uf.unify(2, 3)
        uf.unify(0, 2)
        uf.unify(3, 4)

        for i in range(num_vertices):
            self.assertEqual(uf.parent(i), 0)
        self.assertEqual(uf.num_components, 1)


def sort_fun(x: Tuple[Any, int]) -> int:
    return x[1]


class TestPriorityQueue(unittest.TestCase):

    def test_priority_queue(self):
        pq = PriorityQueue(sort_fun)
        pq.enqueue((0, 5))
        pq.enqueue((1, 3))
        pq.enqueue((2, 6))
        self.assertEqual(len(pq), 3)
        self.assertEqual(pq.dequeue()[0], 1)
        pq.enqueue((4, 1))
        self.assertEqual(pq.dequeue()[0], 4)
        self.assertEqual(pq.dequeue()[0], 0)
        self.assertEqual(pq.dequeue()[0], 2)


class TestIndexedPriorityQueue(unittest.TestCase):

    def test_indexed_priority_queue(self):
        ipq = IndexedPriorityQueue(sort_fun)
        ipq.enqueue(0, ((0, 1), 5))
        ipq.enqueue(1, ((1, 2), 2))
        ipq.enqueue(2, ((2, 3), 6))
        ipq.enqueue(0, ((0, 4), 7))
        self.assertEqual(len(ipq), 3)
        self.assertTrue(0 in ipq)
        self.assertEqual(ipq[0][1], 7)
        self.assertTrue(1 in ipq)
        self.assertTrue(2 in ipq)
        element = ipq.dequeue()
        self.assertEqual(element[0], 1)
        self.assertFalse(1 in ipq)
        element = ipq.dequeue()
        self.assertEqual(element[0], 2)
        self.assertFalse(2 in ipq)
        element = ipq.dequeue()
        self.assertEqual(element[0], 0)
        self.assertFalse(0 in ipq)
        self.assertEqual(len(ipq), 0)
    

class TestSparseTable(unittest.TestCase):

    def test_overlap_friendly_sparse_table(self):
        sparse_table = SparseTableOverlapFriendly([4, 2, 3, 7, 1, 5, 3, 3, 9, 6, 7, -1, 4], lambda x, y: min(x, y))
        self.assertAlmostEqual(sparse_table.query(1, 11), -1, 3)
        self.assertAlmostEqual(sparse_table.query(2, 7), 1, 3)
        self.assertAlmostEqual(sparse_table.query(3, 5), 1, 3)

    def test_associative_sparse_table(self):
        sparse_table = SparseTableAssociative([1, 2, -3, 2, 4, -1, 5], lambda x, y: x * y)
        result1 = sparse_table.query(0, 6)
        self.assertIsNotNone(result1)
        if result1 is not None:
            self.assertAlmostEqual(result1, 240, 3)
        result2 = sparse_table.query(1, 5)
        self.assertIsNotNone(result2)
        if result2 is not None:
            self.assertAlmostEqual(result2, 48, 3)
        

if __name__ == "__main__":
    unittest.main()
