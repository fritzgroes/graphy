import unittest
from graphy.tree import TreeNode, are_isomorphic, center, count_nodes, find, lowest_common_ancestor, root_tree, directed_leaf_sum, height, \
    kruskal_min_span_tree, prims_lazy_min_span_tree, prims_eager_min_span_tree


def get_adj_list1():
    return [[1, 2], [3, 4], [7, 8, 9], [5, 6], [], [], [], [], [10], [], []]


def get_values1():
    return [5, 4, 3, 1, -6, 2, 9, 0, 7, -4, 8]


class TestTreeProperties(unittest.TestCase):

    def test_leaf_sum(self):
        self.assertEqual(directed_leaf_sum(get_adj_list1(), get_values1(), 0), 9)

    def test_height(self):
        self.assertEqual(height(get_adj_list1(), get_values1(), 0), 3)


class TestSearch(unittest.TestCase):

    def test_find_root(self):
        adj_list1 = [[1, 2, 5], [0], [0, 3], [2], [5], [0, 4, 6], [5]]
        root = root_tree(adj_list1, 0)
        self.assertEqual(root.id, 0)
        for child in root.children:
            self.assertIn(child.id, [1, 2, 5])
            if child.id == 1:
                self.assertEqual(len(child.children), 0)
            elif child.id == 2:
                self.assertEqual(len(child.children), 1)
                self.assertEqual(child.children[0].id, 3)
            elif child.id == 5:
                self.assertEqual(len(child.children), 2)
                for child_of_5 in child.children:
                    self.assertIn(child_of_5.id, [4, 6])

    def test_node_count(self):
        adj_list = [[1, 2], [3], [4, 5], [], [6], [], []]
        root = root_tree(adj_list, 0)
        self.assertEqual(count_nodes(root), len(adj_list))

    def test_find_center(self):
        adj_list2 = [[1], [0, 3, 4], [3], [1, 2, 6, 7], [1, 5, 8], [4], [3, 9], [3], [4], [6]]
        self.assertListEqual(sorted(center(adj_list2)), [1, 3])


class TestIsomorphism(unittest.TestCase):

    def test_isomorphism(self):
        adj_list3 = [[1], [0, 2, 4], [1], [4, 5], [1, 3], [3]]
        adj_list4 = [[1], [0, 2], [1, 4], [4], [2, 3, 5], [4]]
        self.assertTrue(are_isomorphic(adj_list3, adj_list4, verbose=True))

class TestMinimumSpanningTree(unittest.TestCase):

    graph = [
        [(1, 5), (3, 4), (4, 1)],
        [(0, 5), (3, 2), (2, 4)],
        [(1, 4), (7, 4), (8, 1), (9, 2)],
        [(0, 4), (1, 2), (4, 2), (5, 5), (6, 11), (7, 2), (8, 4)],
        [(0, 1), (3, 2), (5, 1)],
        [(3, 5), (4, 1), (6, 7)],
        [(3, 11), (5, 7), (7, 1), (8, 4)],
        [(2, 4), (3, 2), (6, 1), (8, 6)],
        [(2, 1), (6, 4), (7, 6), (9, 0)],
        [(2, 2), (8, 0)]
    ]

    def test_kruskal(self):
        min_span_tree_exists, cost, min_span_tree = kruskal_min_span_tree(self.graph)
        self.assertTrue(min_span_tree_exists)
        self.assertAlmostEqual(cost, 14, 3)
        self.assertEqual(len(min_span_tree), len(self.graph) - 1)
    
    def test_lazy_prim(self):
        min_span_tree_exists, cost, min_span_tree = prims_lazy_min_span_tree(self.graph, 0)
        self.assertTrue(min_span_tree_exists)
        self.assertAlmostEqual(cost, 14, 3)
        self.assertEqual(len(min_span_tree), len(self.graph) - 1)

    def test_eager_prim(self):
        min_span_tree_exists, cost, min_span_tree = prims_eager_min_span_tree(self.graph, 0)
        self.assertTrue(min_span_tree_exists)
        self.assertAlmostEqual(cost, 14, 3)
        self.assertEqual(len(min_span_tree), len(self.graph) - 1)


class TestLowestCommonAncestor(unittest.TestCase):

    def test_lca(self):
        adj_list = [[1, 2], [3], [4, 5], [], [6], [], []]
        root = root_tree(adj_list, 0)
        node1 = find(root, 6)
        node2 = find(root, 5)
        self.assertIsInstance(node1, TreeNode)
        self.assertIsInstance(node2, TreeNode)
        if node1 is not None and node2 is not None:
            self.assertEqual(node1.id, 6)
            self.assertEqual(node2.id, 5)
            lca = lowest_common_ancestor(root, node1, node2)
            self.assertIsInstance(lca, TreeNode)
            if lca is not None:
                self.assertEqual(lca.id, 2)


if __name__ == '__main__':
    unittest.main()