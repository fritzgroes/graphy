import unittest
from graphy.search import depth_first, breath_first, bridges, depth_first_gen, strongly_connected_components


class TestSearch(unittest.TestCase):

    def test_depth_first(self):
        graph = [[1, 6, 7], [2, 5], [3, 4], [], [], [], [], [8, 11], [9, 10], [], [], []]
        ordering = depth_first(graph, 0)
        self.assertListEqual(ordering, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])

    def test_depth_first_gen(self):
        graph = [[1, 6, 7], [2, 5], [3, 4], [], [], [], [], [8, 11], [9, 10], [], [], []]
        ordering =[node for node, parent, visited in depth_first_gen(graph, 0)]
        self.assertListEqual(ordering, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])

    def test_breath_first(self):
        graph = [[1, 2, 3], [4, 5], [], [6, 7], [8, 9], [], [10, 11], [], [], [], [], []]
        ordering = breath_first(graph, 0)
        self.assertListEqual(ordering, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])

    def test_bridges_and_articulation_points(self):
        graph = [[1, 2], [0, 2], [0, 1, 5], [2, 4], [3], [2, 6, 8], [5, 7], [6, 8], [5, 7]]
        bs, aps = bridges(graph)
        self.assertListEqual(bs, [(2, 5), (3, 4)])
        self.assertListEqual(aps, [2, 5])

    def test_strongly_connected_components(self):
        graph = [[1], [2], [0], [4, 7], [5], [0, 6], [0, 2, 4], [3, 5]]
        sccs = strongly_connected_components(graph)
        self.assertListEqual(sccs, [[0, 1, 2], [3, 7], [4, 5, 6]])


if __name__ == "__main__":
    unittest.main()