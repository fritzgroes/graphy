import unittest
from graphy.flow import FordFulkersonDfsSolver, EdmondsKarpSolver, CapacityScalingSolver, DinicSolver


class TestMaxFlow(unittest.TestCase):

    flow_graph = [
        [(1, 10), (2, 5), (3, 10)],
        [(4, 10)],
        [(3, 10)],
        [(6, 15)],
        [(2, 20), (7, 15)],
        [(2, 15), (4, 3)],
        [(5, 4), (9, 10)],
        [(8, 10), (10, 15)],
        [(5, 10), (6, 7)],
        [(10, 10)],
        []
    ]

    def test_ford_fulkerson_dfs(self):
        solver = FordFulkersonDfsSolver(0, 10)
        solver.add_from_adj_list(self.flow_graph)
        max_flow = solver.get_max_flow()
        self.assertAlmostEqual(max_flow, 23, 3)

    def test_edmond_karp(self):
        solver = EdmondsKarpSolver(0, 10)
        solver.add_from_adj_list(self.flow_graph)
        max_flow = solver.get_max_flow()
        self.assertAlmostEqual(max_flow, 23, 3)

    def test_capacity_scaling(self):
        solver = CapacityScalingSolver(0, 10)
        solver.add_from_adj_list(self.flow_graph)
        max_flow = solver.get_max_flow()
        self.assertAlmostEqual(max_flow, 23, 3)

    def test_dinic(self):
        solver = DinicSolver(0, 10)
        solver.add_from_adj_list(self.flow_graph)
        max_flow = solver.get_max_flow()
        self.assertAlmostEqual(max_flow, 23, 3)


if __name__ == '__main__':
    unittest.main()