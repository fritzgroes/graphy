from dataclasses import dataclass, field
from typing import Any, Optional
from graphy.datastructures import IndexedPriorityQueue, PriorityQueue, UnionFind
from graphy.path import eulerian_path
from graphy.utils import to_adj_list


@dataclass
class TreeNode:

    id: int
    children: list = field(default_factory=list)

    def serialize(self) -> str:
        return f"({''.join(sorted(child.serialize() for child in self.children))})"


is_directed_leaf = lambda connections: len(connections) == 0

is_leaf = lambda connections: len(connections) <= 1

directed_leaf_sum = lambda graph, values, node: values[node] if is_directed_leaf(graph[node]) \
    else sum(directed_leaf_sum(graph, values, n) for n in graph[node])

height = lambda graph, values, node: 0 if is_directed_leaf(graph[node]) \
    else max(height(graph, values, n) for n in graph[node]) + 1 


def build_tree(graph: list[list[int]], node: TreeNode, parent: Optional[TreeNode]=None) -> TreeNode:
    for child_id in graph[node.id]:
        if parent is not None and child_id == parent.id: continue
        child = TreeNode(child_id)
        node.children.append(child)
        build_tree(graph, child, node)
    return node


root_tree = lambda graph, node_id: build_tree(graph, TreeNode(node_id), None)


def repeat(func, whl):
    
    def inner(arg):
        result = arg
        while whl(result):
            result = func(result)
        return result

    return inner


get_leafs = lambda graph: [node for node, connections in enumerate(graph) if connections is not None and is_leaf(connections)]

remove = lambda graph, nodes: [connections if node not in nodes else None for node, connections in enumerate(graph)]

remaining = lambda graph: [i for i, node in enumerate(graph) if node is not None]

update = lambda graph: [tuple(c for c in connections if graph[c] is not None) 
    if connections is not None else None for connections in graph]

remove_leafs = lambda graph: update(remove(graph, get_leafs(graph)))

not_center = lambda graph: len(remaining(graph)) > 2

center = lambda graph: remaining(repeat(remove_leafs, not_center)(graph))


def are_isomorphic(tree1: list[list[int]], tree2: list[list[int]], verbose=False) -> bool:
    centers1 = center(tree1)
    if verbose: print(f"Found centers indexes for tree 1: {', '.join(map(str, centers1))}")
    centers2 = center(tree2)
    if verbose: print(f"Found centers indexes for tree 2: {', '.join(map(str, centers2))}")

    serialized_tree1 = root_tree(tree1, centers1[0]).serialize()
    if verbose: print(f"Serialized tree 1 using center {centers1[0]}: {serialized_tree1}")

    for c in centers2:
        serialized_tree2 = root_tree(tree2, c).serialize()
        if verbose: print(f"Serialized tree 2 using center {c}: {serialized_tree2}")
        if serialized_tree2 == serialized_tree1:
            return True
    return False


def kruskal_min_span_tree(adj_list_w_weights: list[list[tuple[int, int]]]) -> tuple[bool, int, list[tuple[int, int]]]:
    edge_weights = {}
    for i, edges in enumerate(adj_list_w_weights):
        for j, weight in edges:
            if (i, j) not in edge_weights and (j, i) not in edge_weights:
                edge_weights[(i, j)] = weight
    sorted_weights = sorted(edge_weights.items(), key=lambda x: x[1])
    
    uf = UnionFind(len(adj_list_w_weights))
    min_span_tree = []
    cost = 0
    for ((i, j), weight) in sorted_weights:
        if uf.connected(i, j): continue

        uf.unify(i, j)
        cost += weight
        min_span_tree.append((i, j))

        if uf.all_nodes_connected: break

    min_span_tree_exists = uf.all_nodes_connected
    return min_span_tree_exists, cost, min_span_tree


def sort_fun(x: tuple[Any, int]) -> int:
    return x[1]


def add_edges(node: int, edges: list[tuple[int, int]], visited: list[bool], queue: PriorityQueue) -> None:
    visited[node] = True
    for target, weight in edges:
        if not visited[target]:
            queue.enqueue(((node, target), weight))


def prims_lazy_min_span_tree(adj_list_w_weights: list[list[tuple[int, int]]], start: int) \
        -> tuple[bool, int, list[tuple[int, int]]]:
    num_nodes = len(adj_list_w_weights)
    num_edges = num_nodes - 1
    visited = [False] * num_nodes
    queue = PriorityQueue(sort_fun)
    add_edges(start, adj_list_w_weights[start], visited, queue)
    mst_cost = 0
    mst_edges = []
    while len(queue) > 0 and len(mst_edges) != num_edges:
        edge = queue.dequeue()
        target = edge[0][1]
        if visited[target]: continue
        mst_edges.append(edge[0])
        mst_cost += edge[1]
        add_edges(target, adj_list_w_weights[target], visited, queue)
    return len(mst_edges) == num_edges, mst_cost, mst_edges


def relax_edges(source: int, edges: list[tuple[int, int]], ipq: IndexedPriorityQueue, visited: list[bool]) -> None:
    visited[source] = True
    for target, cost in edges:
        if visited[target]: continue
        if target in ipq:
            ((_, _), current_cost) = ipq[target]
            if cost < current_cost:
                ipq.enqueue(target, ((source, target), cost)) 
        else:
            ipq.enqueue(target, ((source, target), cost))


def prims_eager_min_span_tree(adj_list_w_weights: list[list[tuple[int, int]]], start: int) \
        -> tuple[bool, int, list[tuple[int, int]]]:
    num_nodes = len(adj_list_w_weights)
    num_edges = num_nodes - 1
    visited = [False] * num_nodes
    ipq = IndexedPriorityQueue(sort_fun)
    relax_edges(start, adj_list_w_weights[start], ipq, visited)
    mst_cost = 0
    mst_edges = []
    while len(ipq) > 0 and len(mst_edges) != num_edges:
        target, (edge, cost) = ipq.dequeue()
        mst_edges.append(edge)
        mst_cost += cost
        relax_edges(target, adj_list_w_weights[target], ipq, visited)
    return len(mst_edges) == num_edges, mst_cost, mst_edges


def count_nodes(root: TreeNode) -> int:
    num_nodes = 1
    for child in root.children:
        num_nodes += count_nodes(child)
    return num_nodes


def create_edges(node: TreeNode) -> list[tuple[int, int]]:
    edges = []
    for child in node.children:
        edges.append((node.id, child.id))
        edges.append((child.id, node.id))
        edges += create_edges(child)
    return edges


def calc_depths(path: list[int], root: TreeNode) -> list[int]:

    def rec_depths(path: list[int], node: TreeNode, depth: int, depths: list[int]) -> None:
        for idx in (idx for idx, n in enumerate(path) if n == node.id):
            depths[idx] = depth
        for child in node.children:
            rec_depths(path, child, depth + 1, depths)

    depths = [0] * len(path)
    rec_depths(path, root, 0, depths)
    return depths


def find(root: TreeNode, node: int) -> Optional[TreeNode]:
    for child in root.children:
        if child.id == node:
            return child
        else:
            found = find(child, node)
            if found is not None:
                return found


def lowest_common_ancestor(root: TreeNode, node1: TreeNode, node2: TreeNode) -> Optional[TreeNode]:
    path = eulerian_path(to_adj_list(create_edges(root)), root.id)
    depths = calc_depths(path, root)
    idx1 = path.index(node1.id)
    idx2 = path.index(node2.id)
    idx1, idx2 = sorted([idx1, idx2])
    lowest_depth = min(*depths[idx1:idx2: + 1])
    idx = depths[idx1:idx2 + 1].index(lowest_depth) + idx1
    return find(root, path[idx])
