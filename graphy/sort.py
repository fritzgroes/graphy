from random import choice
from .search import depth_first


def topological(graph: list[list[int]]) -> list[int]:
    visited = [False] * len(graph)
    ordering = []
    while not all(visited):
        start = choice([i for i, v in enumerate(visited) if not v])
        ordering += depth_first(graph, start, reversed=True, visited=visited)
    return list(reversed(ordering))
