from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from math import sqrt
from queue import Queue
from collections import deque
from typing import Optional


INFINITY = float("inf")

@dataclass
class Edge:
    source: int 
    target: int
    capacity: float
    flow: float = field(default=0, init=False)
    residual: Optional["Edge"] = field(default=None, init=False, repr=False)

    @property
    def is_residual(self) -> bool:
        return self.capacity == 0

    @property
    def remaining_capacity(self) -> float:
        return self.capacity - self.flow

    def set_residual(self, residual: "Edge") -> None:
        self.residual = residual

    def augment(self, bottleneck: float) -> None:
        self.flow += bottleneck
        if self.residual is not None: self.residual.flow -= bottleneck


class FlowSolver(ABC):

    def __init__(self, source: int, target: int) -> None:
        self._source = source
        self._target = target
        self._solved = False
        self._max_flow = 0
        self._graph = []
        self._visited = []
        self._visited_token = 0

    @property
    def _size(self) -> int:
        return len(self._graph)

    def add_edge(self, source: int, target: int, capacity: float) -> None:
        if capacity <= 0: raise ValueError("Forward edge capacity <= 0")
        forward = Edge(source, target, capacity)
        residual = Edge(target, source, 0)
        forward.set_residual(residual)
        residual.set_residual(forward)
        while source >= self._size or target >= self._size:
            self._graph.append([])
        self._graph[source].append(forward)
        self._graph[target].append(residual)
        self._solved = False

    def add_from_adj_list(self, adj_list: list) -> None:
        for source, edges in enumerate(adj_list):
            for target, capacity in edges:
                self.add_edge(source, target, capacity)

    def _execute(self) -> None:
        if self._solved: return
        self._visited = [self._visited_token] * self._size
        self._mark_all_nodes_as_unvisited()
        self.solve()
        self._solved = True

    def get_graph(self) -> list:
        self._execute()
        return self._graph

    def get_max_flow(self) -> float:
        self._execute()
        return self._max_flow

    def _visit(self, node: int) -> None:
        self._visited[node] = self._visited_token

    def _is_visited(self, node: int) -> bool:
        return self._visited[node] == self._visited_token

    def _mark_all_nodes_as_unvisited(self) -> None:
        self._visited_token += 1

    @abstractmethod
    def solve(self) -> None:
        raise NotImplementedError


class FordFulkersonDfsSolver(FlowSolver):

    def solve(self) -> None:
        while (flow := self._dfs(self._source, INFINITY)) != 0:
            self._mark_all_nodes_as_unvisited()
            self._max_flow += flow

    def _dfs(self, node: int, flow: float) -> float:
        if node == self._target: return flow
        self._visit(node)
        for edge in self._graph[node]:
            if not self._is_visited(edge.target) and edge.remaining_capacity > 0:
                bottleneck = self._dfs(edge.target, min(flow, edge.remaining_capacity))
                if bottleneck > 0:
                    edge.augment(bottleneck)
                    return bottleneck
        return 0


class EdmondsKarpSolver(FlowSolver):

    def solve(self) -> None:
        while True:
            self._mark_all_nodes_as_unvisited()
            flow = self._bfs()
            self._max_flow += flow
            if flow == 0: break

    def _bfs(self) -> float:
        queue = Queue()
        self._visit(self._source)
        queue.put(self._source)

        prev = [None] * self._size
        while not queue.empty():
            node = queue.get()
            if node == self._target: break
            for edge in self._graph[node]:
                if not self._is_visited(edge.target) and edge.remaining_capacity > 0:
                    prev[edge.target] = edge
                    queue.put(edge.target)
                    self._visit(edge.target)

        if prev[self._target] is None: return 0

        bottleneck = INFINITY
        edge = prev[self._target]
        while edge is not None:
            bottleneck = min(bottleneck, edge.remaining_capacity)
            edge = prev[edge.source]

        edge = prev[self._target]
        while edge is not None:
            edge.augment(bottleneck)
            edge = prev[edge.source]

        return bottleneck
    

class CapacityScalingSolver(FlowSolver):

    def __init__(self, source: int, target: int, min_flow_delta: float=1) -> None:
        super().__init__(source, target)
        self._max_capacity = 0
        self._delta = 0
        self._min_flow_delta = min_flow_delta

    def add_edge(self, source: int, target: int, capacity: float) -> None:
        super().add_edge(source, target, capacity)
        self._max_capacity = max(self._max_capacity, capacity)

    def solve(self) -> None:
        self._delta = int(sqrt(self._max_capacity))**2
        self._flow = 0
        while self._delta >= self._min_flow_delta:
            while True:
                self._mark_all_nodes_as_unvisited()
                flow = self._dfs(self._source, INFINITY) 
                self._max_flow += flow
                if flow == 0: break
            self._delta /= 2

    def _dfs(self, node: int, flow: float) -> float:
        if node == self._target: return flow
        self._visit(node)
        for edge in self._graph[node]:
            if not self._is_visited(edge.target) and edge.remaining_capacity >= self._delta:
                bottleneck = self._dfs(edge.target, min(flow, edge.remaining_capacity)) 
                if bottleneck > 0:
                    edge.augment(bottleneck)
                    return bottleneck
        return 0


class DinicSolver(FlowSolver):

    def __init__(self, source: int, target: int) -> None:
        super().__init__(source, target)
        self._levels: list[int] = []

    def solve(self) -> None:
        while self._bfs():
            next = [0] * self._size
            while (flow := self._dfs(self._source, next, INFINITY)) != 0:
                self._max_flow += flow

    def _bfs(self) -> bool:
        self._levels[:] = [-1] * self._size
        queue = deque()
        queue.append(self._source)
        self._levels[self._source] = 0
        while len(queue) > 0:
            node = queue.popleft()
            for edge in self._graph[node]:
                if self._levels[edge.target] == -1 and edge.remaining_capacity > 0:
                    self._levels[edge.target] = self._levels[node] + 1
                    queue.append(edge.target)
        return self._levels[self._target] != -1

    def _dfs(self, node: int, next: list[int], flow: float) -> float:
        if node == self._target: return flow
        num_edges = len(self._graph[node])
        while next[node] < num_edges:
            edge = self._graph[node][next[node]]
            if self._levels[edge.target] == self._levels[node] + 1 and edge.remaining_capacity > 0:
                bottleneck = self._dfs(edge.target, next, min(flow, edge.remaining_capacity))
                if bottleneck > 0:
                    edge.augment(bottleneck)
                    return bottleneck
            next[node] += 1
        return 0
