from queue import Queue
from random import choice
from typing import Callable, Optional, Generator


def depth_first(graph: list[list[int]], node: int, reversed: bool=False, visited: Optional[list[bool]]=None) -> list[int]:
    if visited is None: visited = [False] * len(graph)
    if visited[node]: return []
    visited[node] = True
    ordering = []
    if not reversed:
        ordering.append(node)
    for child in graph[node]:
        ordering += depth_first(graph, child, reversed=reversed, visited=visited)
    if reversed:
        ordering.append(node)
    return ordering


def depth_first_gen(graph: list[list[int]], node: int, parent: Optional[int]=None, visited: Optional[list[bool]]=None) \
        -> Generator[tuple[int, Optional[int], bool], None, None]:
    if visited is None: visited = [False] * len(graph)
    yield node, parent, visited[node]
    if not visited[node]:
        visited[node] = True
        for child in graph[node]:
            yield from depth_first_gen(graph, child, parent=node, visited=visited)


def breath_first(graph: list[list[int]], node: int, visited=None) -> list[int]:
    if visited is None: visited = [False] * len(graph)
    if visited[node]: return []
    queue = Queue()
    queue.put(node)
    ordering = [node]
    while not queue.empty():
        next_node = queue.get()
        for child in graph[next_node]:
            if not visited[child]:
                ordering.append(child)
                queue.put(child)
                visited[child] = True
    return ordering


def depth_first_bridges(graph: list[list[int]], ids: list[int], lows: list[int], id: int, visited: list[bool], \
        is_art_point: list[bool], out_edge_count: int, root: int, at: int, parent: Optional[int], bridges: list[tuple[int, int]]) \
        -> None:
    if parent == root: out_edge_count += 1
    visited[at] = True
    id += 1
    ids[at] = id
    lows[at] = id
    for to in graph[at]:
        if to == parent: continue
        if not visited[to]:
            depth_first_bridges(graph, ids, lows, id, visited, is_art_point, out_edge_count, root, to, at, bridges)
            lows[at] = min(lows[at], lows[to])
            if ids[at] < lows[to]:
                bridges.append((at, to))
            if ids[at] <= lows[to]:
                is_art_point[at] = True
        else:
            lows[at] = min(lows[at], ids[to])


def bridges(graph: list[list[int]]) -> tuple[list[tuple[int, int]], list[int]]:
    size = len(graph)
    id = 0
    out_edge_count = 0
    ids = [0] * size
    lows = [0] * size
    visited = [False] * size
    is_art_point = [False] * size
    bridges = []
    for i in range(size):
        if not visited[i]:
            out_edge_count = 0
            depth_first_bridges(graph, ids, lows, id, visited, is_art_point, out_edge_count, i, i, None, bridges)
            is_art_point[i] = (out_edge_count > 1)
    return bridges, [i for i, point in enumerate(is_art_point) if point]


def depth_first_map(graph: list[list[int]], node: int, parent: Optional[int]=None, \
        before: Optional[Callable[[int, Optional[int], bool], None]]=None, after=None, visited=None):
    if visited is None: visited = [False] * len(graph)

    was_visited = visited[node]
    if before is not None: before(node, parent, was_visited)
    if not visited[node]: 
        visited[node] = True
        for child in graph[node]:
            depth_first_map(graph, child, parent=node, before=before, after=after, visited=visited)
    if after is not None: after(node, parent, was_visited)


def opt_min(val1: Optional[int], val2: Optional[int]) -> Optional[int]:
    if val1 is None: return val2
    if val2 is None: return val1
    return min(val1, val2)


def strongly_connected_components(graph: list[list[int]]) -> list[list[int]]:
    size = len(graph)
    visited = [False] * size

    stack = []
    ids: list[Optional[int]] = [None] * size
    lows: list[Optional[int]] = [None] * size
    cid = 0

    def before(node, parent, is_visited):
        nonlocal cid
        if not is_visited:
            stack.append(node)
            ids[node] = cid
            lows[node] = cid
            cid += 1

    def after(node, parent, is_visited): 
        nonlocal stack
        if parent is not None: 
            if node in stack:
                if lows[parent] is not None:
                    lows[parent] = opt_min(lows[parent], lows[node])
            if ids[parent] == lows[node]: 
                stack = [id for id in stack if lows[id] != lows[node]]

    def lows_to_sccs(lows: list[Optional[int]]) -> list[list[int]]:
        sccs = {}
        for i, low in enumerate(lows):
            if low not in sccs:
                sccs[low] = []
            sccs[low].append(i)
        return list(sccs.values())

    while not all(visited):
        start = choice([i for i, v in enumerate(visited) if not v])
        depth_first_map(graph, start, before=before, after=after, visited=visited)

    return lows_to_sccs(lows)
