from typing import Callable, Generic, Optional, TypeVar
from math import log
from numpy import empty, nan


class UnionFind:

    def __init__(self, size: int) -> None:
        self._parents = [i for i in range(size)]
        self._sizes = [1 for _ in range(size)]

    @property
    def size(self) -> int:
        return len(self._parents)

    @property
    def num_components(self) -> int:
        return sum(s > 0 for s in self._sizes)

    @property
    def all_nodes_connected(self):
        return self.num_components == 1

    def parent(self, node: int) -> int:
        return self._parents[node]

    def find(self, node: int) -> int:
        if node == self._parents[node]: return node
        self._parents[node] = self.find(self._parents[node])
        return self._parents[node] 
        
    def unify(self, node1: int, node2: int) -> None:
        if self.connected(node1, node2): return
        
        root1 = self.find(node1)
        root2 = self.find(node2)

        if self._sizes[root1] < self._sizes[root2]:
            smaller, larger = root1, root2
        else: 
            smaller, larger = root2, root1
        self._parents[smaller] = larger
        self._sizes[larger] += self._sizes[smaller]
        self._sizes[smaller] = 0

    def connected(self, node1: int, node2: int) -> bool:
        return self.find(node1) == self.find(node2)


K, V = TypeVar("K"), TypeVar("V")


class PriorityQueue(Generic[V]):

    def __init__(self, sort_fun: Callable[[V], float]) -> None:
        self._queue: list[V] = []
        self._sort_fun = sort_fun

    def __len__(self) -> int:
        return len(self._queue)

    def __repr__(self) -> str:
        return self._queue.__repr__()

    def enqueue(self, value: V) -> None:
        self._queue.append(value)

    def dequeue(self) -> V:
        self._queue.sort(key=self._sort_fun, reverse=True)
        return self._queue.pop()


class IndexedPriorityQueue(Generic[K, V]):

    def __init__(self, sort_fun: Callable[[V], int]) -> None:
        self._queue: dict[K, V] = {}
        self._sort_fun = sort_fun

    def __len__(self) -> int:
        return len(self._queue)

    def __repr__(self) -> str:
        return self._queue.__repr__()

    def __contains__(self, key: K) -> bool:
        return key in self._queue
    
    def __getitem__(self, key: K) -> V:
        return self._queue[key]

    def enqueue(self, key: K, value: V) -> None:
        self._queue[key] = value

    def dequeue(self) -> tuple[K, V]:
        kv = sorted(self._queue.items(), key=lambda kv: self._sort_fun(kv[1]))[0]
        self._queue.pop(kv[0])
        return kv

    
class SparseTable:
    
    def __init__(self, input: list[float], fun: Callable[[float, float], float]) -> None:
        N = len(input)
        P = int(log(N) / log(2))
        self._fun = fun

        self._table = empty((P + 1, N))
        self._table[:] = nan
        self._table[0, :] = input

        for p in range(1, P + 1):
            i = 0
            while i + 2 ** p <= N:
                x = self._table[p - 1, i]
                y = self._table[p - 1, i + 2 ** (p - 1)]
                self._table[p, i] = self._fun(x, y)
                i += 1

    @property
    def table(self):
        return self._table

    
class SparseTableOverlapFriendly(SparseTable):

    def query(self, i: int, j: int) -> float:
        p = int(log(j - i + 1) / log(2))
        x = self._table[p, i]
        y = self._table[p, j - 2 ** p + 1]
        return self._fun(x, y)


class SparseTableAssociative(SparseTable):

    def query(self, i: int, j: int) -> Optional[float]:
        result = None
        while i <= j:
            p = int(log(j - i + 1) / log(2))
            if result is None:
                result = self._table[p, i]
            else: 
                result = self._fun(result, self._table[p, i])
            i += 2 ** p
        return result
