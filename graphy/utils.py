wo_weights = lambda graph: [list(connection[0] for connection in connections) for connections in graph]

map_to_connection = lambda func, graph: [list(func(connection) for connection in connections) for connections in graph]

map_to_node_ids = lambda func, graph: map_to_connection(lambda connection: list((func(connection[0]), connection[1])), graph)

map_to_weights = lambda func, graph: map_to_connection(lambda connection: list((connection[0], func(connection[1]))), graph)


def to_adj_list(edges: list[tuple[int, int]]) -> list[list[int]]:
    size = max(max(edge) for edge in edges) + 1
    adj_list = [[] for _ in range(size)]
    for edge in edges:
        adj_list[edge[0]].append(edge[1])
    return adj_list
