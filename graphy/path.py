from itertools import chain, combinations, groupby, permutations, starmap
from typing import Any, Optional, Sequence
from numpy import array, ndarray, zeros, empty, nan

from graphy.datastructures import PriorityQueue
from .utils import wo_weights, map_to_weights
from .sort import topological


INFINITY = float("inf")
NEGATIVE_INFINITY = float("-inf")
NEGATIVE_CYCLE = -1

to_gen = lambda dic: (dic[i] if i in dic else 0 for i in range(max(dic) + 1))

out_degrees = lambda graph: (len(edges) for edges in graph)

in_degrees = lambda graph: to_gen(dict(starmap(lambda i, group: (i, len(list(group))), groupby(sorted(chain.from_iterable(graph))))))

calc_degree_diffs = lambda graph: map(lambda i, o: o - i, in_degrees(graph), out_degrees(graph))

is_eulerian_circuit = lambda graph: all(map(lambda d: d == 0, calc_degree_diffs(graph)))

all_in_range = lambda graph: all(map(lambda d: abs(d) < 2, calc_degree_diffs(graph)))
count_diffs = lambda graph, diff: sum(map(lambda d: d == diff, calc_degree_diffs(graph)))

is_eulerian_path = lambda graph: is_eulerian_circuit(graph) \
    or all_in_range(graph) and count_diffs(graph, 1) < 2 and count_diffs(graph, -1) < 2


def single_source_shortest_path(graph: list[list[tuple[int, float]]]) -> list[float]:
    graph_ordering = topological(wo_weights(graph))
    shortest_paths = [INFINITY] * len(graph_ordering)
    shortest_paths[graph_ordering[0]] = 0
    for source_node in graph_ordering:
        for node, weight in graph[source_node]:
            shortest_paths[node] = min(shortest_paths[node], shortest_paths[source_node] + weight)
    return shortest_paths


def single_source_longest_path(graph: list[list[tuple[int, float]]]) -> list[float]:
    inverse = lambda weight: -weight
    inverse_graph = map_to_weights(inverse, graph)
    shortest_path = single_source_shortest_path(inverse_graph)
    return list(map(inverse, shortest_path))


def sort_fun(x: tuple[Any, float]) -> float:
    return x[1]


def dijkstra_distances(graph: list[list[tuple[int, float]]], start: int, end: Optional[int]=None, verbose: bool=False) \
        -> tuple[list[float], list[Optional[int]]]:
    num = len(graph)
    visited = [False] * num
    previous: list[Optional[int]] = [None] * num
    distances = [INFINITY] * num
    distances[start] = 0
    queue = PriorityQueue(sort_fun)
    queue.enqueue((0, 0))
    while len(queue) > 0:
        if verbose: print(queue)
        node, distance = queue.dequeue()
        visited[node] = True
        if distances[node] < distance: continue
        for n, weight in graph[node]:
            if visited[n]: continue
            new_distance = distances[node] + weight
            if new_distance < distances[n]:
                previous[n] = node
                distances[n] = new_distance
                queue.enqueue((n, new_distance))
        if verbose: print(distances)
        if end is not None and node == end: break
    return distances, previous


def dijkstra_shortest(graph: list[list[tuple[int, float]]], start: int, end: int, verbose: bool=False) -> list[float]:
    distances, previous = dijkstra_distances(graph, start, end=end, verbose=verbose)
    path = []
    if (distances[end] == INFINITY): return path
    at = end
    while (at is not None):
        path.append(at)
        at = previous[at]
    return list(reversed(path))


def bellman_ford_shortest(graph: list[list[tuple[int, float]]], start: int, distances: Optional[list[float]]=None) \
        -> list[float]:
    if distances is None: 
        distances = [INFINITY] * len(graph)
        distances[start] = 0
    for i in range(0, len(graph) - 1):
        for node_from, nodes_to in enumerate(graph):
            for node_to, weight in nodes_to:
                if (distances[node_from] + weight < distances[node_to]):
                    distances[node_to] = distances[node_from] + weight
    for i in range(0, len(graph) - 1):
        for node_from, nodes_to in enumerate(graph):
            for node_to, weight in nodes_to:
                if (distances[node_from] + weight < distances[node_to]):
                    distances[node_to] = NEGATIVE_INFINITY
    return distances


def floyd_warshall_shortest(adj_matrix: list[list[int]] | ndarray) \
        -> tuple[ndarray, ndarray, ndarray, ndarray]:

    def setup_floyd_warshall_data(adj_matrix: ndarray) -> tuple[ndarray, ndarray]:
        distances = adj_matrix.copy()
        next_nodes = empty(adj_matrix.shape)
        next_nodes[:] = nan
        size = adj_matrix.shape[0]
        for i in range(size):
            for j in range(size):
                if distances[i, j] != INFINITY:
                    next_nodes[i, j] = j
        return distances, next_nodes

    def propagate_negative_cycles(distances: ndarray, next_nodes: ndarray) -> tuple[ndarray, ndarray]:
        negative_cycles = distances.copy()
        new_next_nodes = next_nodes.copy()
        for k in range(size):
            for i in range(size):
                for j in range(size):
                    if negative_cycles[i, k] < INFINITY and negative_cycles[k, j] < INFINITY \
                            and negative_cycles[i, k] + negative_cycles[k, j] < negative_cycles[i, j]:
                        negative_cycles[i, j] = NEGATIVE_INFINITY
                        new_next_nodes[i, j] = NEGATIVE_CYCLE
        return negative_cycles, new_next_nodes

    adj_matrix = array(adj_matrix)
    assert len(adj_matrix.shape) == 2
    assert adj_matrix.shape[0] == adj_matrix.shape[1]

    distances, next_nodes = setup_floyd_warshall_data(adj_matrix)
    
    size = adj_matrix.shape[0]
    for k in range(size):
        for i in range(size):
            for j in range(size):
                new_distance = distances[i, k] + distances[k, j]
                if new_distance < distances[i, j]:
                    distances[i, j] = new_distance
                    next_nodes[i, j] = next_nodes[i, k]

    negative_cycles, new_next_nodes = propagate_negative_cycles(distances, next_nodes)
    
    return distances, next_nodes, negative_cycles, new_next_nodes


def reconstruct_floyd_warshall_path(distances: ndarray, next_nodes: ndarray, start: int, end: int) -> Optional[list[int]]:
    path = []
    if distances[start, end] == INFINITY: return path
    at = int(start)
    while at != end:
        if at == NEGATIVE_CYCLE: return None
        path.append(at)
        at = int(next_nodes[at, end])
    if next_nodes[at, end] == -1: return None
    path.append(end)
    return path


def shortest_route_bf(adj_mat: list[list[float]], start: int, verbose: bool=False) -> tuple[float, list[int]]:
    weights = array(adj_mat)
    shortest_routes = []
    min = float("inf")
    rest = list(range(4))
    rest.remove(start)
    for permutation in permutations(rest):
        path = [start] + list(permutation) + [start]
        weight_sum = 0
        for s, e in zip(path[0:-1], path[1:]):
            weight_sum += weights[s, e]
        if weight_sum < min:
            min = weight_sum
            shortest_routes = path
        if verbose: print(path, weight_sum)
    return min, shortest_routes


def shortest_route(adj_mat: list[list[float]] | ndarray, start) -> tuple[float, list[int]]:

    def setup(weights: ndarray, memo: ndarray, start: int) -> None:
        for i in range(weights.shape[0]):
            if i == start: continue
            memo[i, 1 << start | 1 << i] = weights[start, i]

    def not_in(node: int, state: int) -> bool:
        return ((1 << node) & state) == 0

    def to_state(combination: Sequence[int]) -> int:
        state = 0
        for i in combination:
            state += (1 << i)
        return state

    def solve(weights: ndarray, memo: ndarray, start: int) -> None:
        N = weights.shape[0]
        for r in range(3, N + 1):
            for combination in combinations(range(N), r):
                subset = to_state(combination)
                if not_in(start, subset): continue
                for next in range(N):
                    if next == start or not_in(next, subset): continue
                    state = subset ^ (1 << next)
                    min_dist = float("inf")
                    for end in range(N):
                        if end == start or end == next or not_in(end, subset): continue
                        new_dist = memo[end, state] + weights[end, next]
                        if new_dist < min_dist:
                            min_dist = new_dist
                    memo[next, subset] = min_dist

    def find_min_cost(weights: ndarray, memo: ndarray, start: int) -> float:
        N = weights.shape[0]
        end_state = (1 << N) - 1
        min_cost = float("inf")
        for end in range(N):
            if end == start: continue
            tour_cost = memo[end, end_state] + weights[end, start]
            if tour_cost < min_cost:
                min_cost = tour_cost
        return min_cost

    def find_optimal_tour(weights: ndarray, memo: ndarray, start: int) -> list[int]:
        N = weights.shape[0]
        last_index = start
        state = (1 << N) - 1
        tour = [0] * (N + 1)

        for i in range(N - 1, 0, -1):
            index = -1
            for j in range(N):
                if j == start or not_in(j, state): continue
                if index == -1: index = j
                prev_dist = memo[index, state] + weights[index, last_index]
                new_dist = memo[j, state] + weights[j, last_index]
                if new_dist < prev_dist: 
                    index = j
            tour[i] = index
            state = state ^ (1 << index)
            last_index = index
        tour[0], tour[N] = start, start
        return tour

    weights = array(adj_mat)
    N = weights.shape[0]
    memo = zeros((N, 2**N))

    setup(weights, memo, start)
    solve(weights, memo, start)
    min_cost = find_min_cost(weights, memo, start)
    tour = find_optimal_tour(weights, memo, start)
    return min_cost, tour


def find_start_node(graph: list[list[int]]) -> int:
    start = 0
    try:
        return list(calc_degree_diffs(graph)).index(1) 
    except ValueError:
        pass
    try: 
        return list(map(lambda d: d > 0, out_degrees(graph))).index(True)
    except ValueError:
        pass
    return start


def eulerian_dfs(graph: list[list[int]], node: int, outs: list[int], path: Optional[list[int]]=None) -> list[int]:
    if path is None:
        path = []
    while outs[node] > 0:
        outs[node] -= 1
        next = graph[node][outs[node]]
        path = eulerian_dfs(graph, next, outs, path=path)
    path.insert(0, node)
    return path


def eulerian_path(graph: list[list[int]], start: Optional[int]=None) -> list[int]:
    if start is None:
        start = find_start_node(graph)
    outs = list(out_degrees(graph))
    if not is_eulerian_path(graph): return []
    path = eulerian_dfs(graph, start, outs=outs)
    if len(path) == len(list(chain.from_iterable(graph))) + 1: return path
    return []
