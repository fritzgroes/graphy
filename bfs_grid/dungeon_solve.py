def read_data(dungeon_file):
    data = []
    with open(dungeon_file, "r") as file:
        for line in file:
            data.append(line[:-1])
    return data


def find_start(data, start):
    i = 0
    for row in data:
        j = 0
        for field in row:
            if field == start:
                return i, j
            j += 1
        i += 1


def solve(map, start, end, blocked):
    start_loc = find_start(map, start)

    R = len(map)
    C = len(map[0])
    visited = [[False] * C for i in range(R)]
    reached_end = False

    dr = [-1,  1,  0,  0]
    dc = [0,  0,  1, -1]

    visited[start_loc[0]][start_loc[1]] = True
    queue = []
    step = 0
    queue.append((*start_loc, step))
    while len(queue) > 0:
        r, c, step = queue.pop(0)
        if map[r][c] == end:
            reached_end = True
            break

        for i in range(4):
            rr, cc = r + dr[i], c + dc[i]

            if rr < 0 or rr >= R: continue
            if cc < 0 or cc >= C: continue

            if visited[rr][cc]: continue
            if map[rr][cc] == blocked: continue

            queue.append((rr, cc, step + 1))
            visited[rr][cc] = True

    if reached_end:
        return step


if __name__ == "__main__":
    dungeon_file = "bfs_grid\dungeon0.txt"
    #dungeon_file = "bfs_grid\dungeon1.txt"
    start_char = "S"
    end_char = "E"
    blocked_char = "#"

    data = read_data(dungeon_file)
    for row in data:
        print(row)

    steps = solve(data, start_char, end_char, blocked_char)
    print(steps)
