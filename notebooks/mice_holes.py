from dataclasses import dataclass
from math import sqrt
from typing import List, Tuple
from graphy.flow import FordFulkersonDfsSolver


@dataclass
class Position:
    x: float
    y: float

    def distance(self, position: "Position") -> float: 
        return sqrt((position.x - self.x)**2 + (position.y - self.y)**2)


@dataclass
class Mouse:
    position: Position


@dataclass
class Hole:
    position: Position
    capacity: float


def solve(mice: List[Mouse], holes: List[Hole], radius: float) -> List[Tuple[int, int]]:
    M = len(mice)
    H = len(holes)
    source = 0
    target = 1
    solver = FordFulkersonDfsSolver(source, target)
    mouse_offset = 2
    for i, _ in enumerate(mice):
        solver.add_edge(source, i + mouse_offset, 1)
    hole_offset = 2 + M
    for i, hole in enumerate(holes):
        solver.add_edge(i + hole_offset, target, hole.capacity) 
    for i, mouse in enumerate(mice):
        for j, hole in enumerate(holes):
            if mouse.position.distance(hole.position) < radius:
                solver.add_edge(i + mouse_offset, j + hole_offset, 1)
    mouse_holes = []
    for edges in solver.get_graph():
        for edge in edges:
            if not edge.is_residual:
                if 2 <= edge.source < hole_offset and edge.flow > 0:
                    mouse_holes.append((edge.source - mouse_offset, edge.target - hole_offset))
    return mouse_holes


if __name__ == '__main__':
    mice_data = [(1, 0), (0, 1), (8, 1), (12, 0), (12, 4), (15, 5)]
    holes_data = [(1, 1, 1), (10, 2, 2), (14, 5, 1)]
    radius = 3
    mice = []
    for x, y in mice_data:
        mice.append(Mouse(Position(x, y)))
    holes = []
    for x, y, capacity in holes_data:
        holes.append(Hole(Position(x, y), capacity))
    mouse_holes = solve(mice, holes, radius)
    for mouse_idx, hole_idx in mouse_holes:
        print(f"{mice[mouse_idx]} hides in {holes[hole_idx]}")
